// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBL5vZkyRlyFNMLDe1oypIBWyxHKFKjqQw',
    authDomain: 'socialize-lite.firebaseapp.com',
    databaseURL: 'https://socialize-lite.firebaseio.com',
    projectId: 'socialize-lite',
    storageBucket: '',
    messagingSenderId: '78504992088',
    appId: '1:78504992088:web:cc5334f38d54fee9'
  }
};
